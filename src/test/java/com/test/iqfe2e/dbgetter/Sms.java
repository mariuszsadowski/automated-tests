package com.test.iqfe2e.dbgetter;

import org.springframework.jdbc.core.JdbcTemplate;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Sms {

    private static final Pattern urlPattern = Pattern.compile(
            "(?:^|[\\W])((ht|f)tp(s?):\\/\\/|www\\.)"
                    + "(([\\w\\-]+\\.){1,}?([\\w\\-.~]+\\/?)*"
                    + "[\\p{Alnum}.,%_=?&#\\-+()\\[\\]\\*$~@!:/{};']*)",
            Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);

    public static String getLastSms(JdbcTemplate jdbcTemplate, String phoneNumber) {

        String sms =  jdbcTemplate.queryForObject(
                "select params from sms_history where phone_number = ? order by send_date desc limit 1",
                new Object[]{phoneNumber},
                String.class
        );

        String url = null;
        Matcher matcher = urlPattern.matcher(sms);
        if (matcher.find()) {
            url = matcher.group();
        }
        return url;
    }

    public static String getLastSmsOfHelloMessage(JdbcTemplate jdbcTemplate, String phoneNumber) {

        String sms =  jdbcTemplate.queryForObject(
                "select params from sms_history where phone_number = ? order by send_date desc limit 1",
                new Object[]{phoneNumber},
                String.class
        );

        String url = null;
        Matcher matcher = urlPattern.matcher(sms);
        if (matcher.find()) {
            url = matcher.group();
        }
        return url;
    }
}
