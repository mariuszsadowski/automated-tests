package com.test.iqfe2e.dbgetter;

import com.test.iqfe2e.config.VariablesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Email {

    @Autowired
    VariablesService variablesService;

    private static final Pattern urlPattern = Pattern.compile(
            "(?:^|[\\W])((ht|f)tp(s?):\\/\\/|www\\.)"
                    + "(([\\w\\-]+\\.){1,}?([\\w\\-.~]+\\/?)*"
                    + "[\\p{Alnum}.,%_=?&#\\-+()\\[\\]\\*$~@!:/{};']*)",
            Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);

    public static String getLastEmailOfType(JdbcTemplate jdbcTemplate, String email, String emailType, String adminPanelUrl) {

        String emailContent =  jdbcTemplate.queryForObject(
                "select params from email_sent_history where destination = ? and type = ?  order by date desc limit 1",
                new Object[]{email, emailType},
                String.class
        );

        String url = null;
        Matcher matcher = urlPattern.matcher(emailContent);
        if (matcher.find()) {
            url = matcher.group();
        }
        url =url.substring(1);
        return url;
    }
}
