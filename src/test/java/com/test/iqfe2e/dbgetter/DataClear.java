package com.test.iqfe2e.dbgetter;

import org.springframework.jdbc.core.JdbcTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DataClear {

    public List<String> userList;
    public List<String> leadList;
    public List<String> npsList;

    public DataClear() {
        userList = new ArrayList<>();
        leadList = new ArrayList<>();
        npsList = new ArrayList<>();
    }

    public void addLeadToCleanUp(JdbcTemplate jdbcTemplate, String phoneNumber) {
        leadList.add(phoneNumber);
        if (leadExists(jdbcTemplate, phoneNumber)) {
            deleteLead(jdbcTemplate, phoneNumber);
        }
    }

    public void addUserToCleanUp(JdbcTemplate jdbcTemplate, String login) {
        userList.add(login);
        if (userExists(jdbcTemplate, login)) {
            deleteUser(jdbcTemplate, login);
        }
    }

    public void addNpsToCleanUp(JdbcTemplate jdbcTemplate, String phoneNumber) {
        npsList.add(phoneNumber);
        if (npsExists(jdbcTemplate, phoneNumber)) {
            deleteNps(jdbcTemplate, phoneNumber);
        }
    }

    public void clearData(JdbcTemplate jdbcTemplate) {
        userList.forEach(login -> deleteUser(jdbcTemplate, login));
        System.out.println("USERS DELETED: " + Arrays.toString(userList.toArray()));
        leadList.forEach(phoneNumber -> deleteLead(jdbcTemplate, phoneNumber));
        System.out.println("LEADS DELETED: " + Arrays.toString(leadList.toArray()));
        npsList.forEach(phoneNumber -> deleteNps(jdbcTemplate, phoneNumber));
        System.out.println("NPS DELETED: " + Arrays.toString(npsList.toArray()));
    }

    private boolean userExists(JdbcTemplate jdbcTemplate, String login) {
        String sql = "select count(*) from jhi_user WHERE login = ?";
        int count = jdbcTemplate.queryForObject(sql, new Object[]{login}, Integer.class);
        return count > 0;
    }

    private boolean leadExists(JdbcTemplate jdbcTemplate, String phoneNumber) {
        String sql = "select count(*) from data where params ->> 'phoneNumber' = ?";
        int count = jdbcTemplate.queryForObject(sql, new Object[]{phoneNumber}, Integer.class);
        return count > 0;
    }

    private boolean npsExists(JdbcTemplate jdbcTemplate, String phoneNumber) {
        String sql = "select count(*) from sms_history WHERE phone_number = ? and activity_id like'nps%'";
        int count = jdbcTemplate.queryForObject(sql, new Object[]{phoneNumber}, Integer.class);
        return count > 0;
    }

    private void deleteUser(JdbcTemplate jdbcTemplate, String login) {
        jdbcTemplate.execute(
                "delete from jhi_user_authority where user_id in ");

    }

    private void deleteLead(JdbcTemplate jdbcTemplate, String phoneNumber) {
        jdbcTemplate.execute("delete from data where params ->> 'phoneNumber' = '" + phoneNumber + "';");
    }

    private void deleteNps(JdbcTemplate jdbcTemplate, String phoneNumber) {
        jdbcTemplate.execute("delete from sms_history where phone_number = '" + phoneNumber + "' and activity_id like 'nps%';");
    }
}