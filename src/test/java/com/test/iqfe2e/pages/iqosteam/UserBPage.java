

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class UserBPage {

    WebDriver driver;
    WebDriverWait wait;

    public UserBPage(WebDriver driver, WebDriverWait wait) {
        this.driver = driver;
        this.wait = wait;
        PageFactory.initElements(driver, this);
    }

    //Dodaj konsumenta
    @FindBy(xpath = "//span[contains(text(),'Dodaj Konsumenta')]")
    public WebElement addConsumer;

    @FindBy(xpath = "//input[@id='eventCheckbox']")
    public WebElement eventCheckbox;

    @FindBy(xpath = "//span[contains(text(),'Wybierz wydarzenie')]/following-sibling::select")
    public WebElement eventName;

    @FindBy(xpath = "//input[@id='value']")
    public WebElement number;

    @FindBy(xpath = "//div[@id='dropdown']/div[3]")
    public WebElement element;

    @FindBy(xpath = "//input[@id='Trial']")
    public WebElement Trial;

    @FindBy(xpath = "//input[@id='productInfo']")
    public WebElement productInfo;

    @FindBy(xpath = "//span[contains(text(),'Preferencje konsumenta')]/following-sibling::select")
    public WebElement types;

    @FindBy(xpath = "//label[contains(text(), 'Wydanie')]")
    public WebElement webElement;

    @FindBy(xpath = "//input[@id='isLeadNotQuestionnaire']")
    public WebElement isLead;

    @FindBy(xpath = "//input[@id='soldOnSpot']")
    public WebElement sold;

    @FindBy(xpath = "//input[@id='sold']")
    public WebElement soldPos;

    @FindBy(xpath = "//input[@id='sold']")
    public WebElement user;

    @FindBy(xpath = "//input[@id='code']")
    public WebElement Code;

    @FindBy(xpath = "//span[contains(text(),'Imię')]//following-sibling::input")
    public WebElement name;

    @FindBy(xpath = "//span[contains(text(),'Nr telefonu komórkowego')]//following-sibling::input")
    public WebElement phone;

    @FindBy(xpath = "//input[@id='address-input']")
    public WebElement addressInput;

    @FindBy(xpath = "//span[contains(text(),'Sprawdź')]")
    public WebElement addressButton;

    @FindBy(xpath = "//label[contains(text(),'Oświadczam, że znajomy, któremu')]")
    public WebElement agreementCheckbox;

    @FindBy(xpath = "//span[contains(text(),'Wyślij')]")
    public WebElement send;

    @FindBy(xpath = "//button[contains(text(),'Zapisz')]")
    public WebElement save;

    @FindBy(xpath = "//button[contains(text(),'Anuluj')]")
    public WebElement cancel;

    @FindBy(xpath = "//button[contains(text(), 'Ok')]")
    public WebElement leadAddedPopup;

    public void addNewLead(String phoneNumber, boolean event, String eventName, boolean trial, boolean productInfo, String types, String address, boolean type) {

        wait.until(ExpectedConditions.elementToBeClickable(addConsumer));
        addConsumer.click();
        wait.until(ExpectedConditions.visibilityOf(number));

        Actions actions = new Actions(driver);

        actions.moveToElement(isLead);
        actions.perform();
        if (!isLead.isSelected()) {
            isLead.click();
        }

        actions.moveToElement(number);
        actions.perform();
        wait.until(ExpectedConditions.visibilityOf(eventCheckbox));
        if (event){
            if (!eventCheckbox.isSelected()) {
                eventCheckbox.click();
            }
            this.eventName.sendKeys(eventName);
        }
        else {
            if (eventCheckbox.isSelected()) {
                eventCheckbox.click();
            }
            number.sendKeys("112");
            wait.until(ExpectedConditions.visibilityOf(element));
            element.click();
            actions.moveToElement(sold);
            actions.perform();
            if (sold.isSelected()) {
                sold.click();
            }
        }

        actions.moveToElement(this.trial);
        actions.perform();
        if (trial) {
            if (!this.trial.isSelected()) {
                this.trial.click();
            }
        }
        else {
            if (this.trial.isSelected()) {
                this.trial.click();
            }
        }
        if (productInfo) {
            if (!this.productInfo.isSelected()) {
                this.productInfo.click();
            }
        }
        else {
            if (this.productInfo.isSelected()) {
                this.productInfo.click();
            }
        }
        this.types.sendKeys(types);

        actions.moveToElement(name);
        actions.perform();
        name.sendKeys("Testowy lead");

        actions.moveToElement(phone);
        actions.perform();
        phone.sendKeys(phoneNumber);

        actions.moveToElement(addressInput);
        actions.perform();
        addressInput.sendKeys(address);
        addressButton.click();
        wait.until(ExpectedConditions.visibilityOf(addressButton));

        actions.moveToElement(agreementCheckbox);
        actions.perform();
        if (!this.agreementCheckbox.isSelected()) {
            this.agreementCheckbox.click();
        }

        actions.moveToElement(this.webElement);
        actions.perform();
        if (type) {
            if (!this.webElement.isSelected()) {
                this.webElement.click();
            }
        }
        else {
            if (this.webElement.isSelected()) {
                this.webElement.click();
            }
        }

        actions.moveToElement(send);
        actions.perform();
        wait.until(ExpectedConditions.elementToBeClickable(send));
        send.click();
        wait.until(ExpectedConditions.visibilityOf(cancel));
        wait.until(ExpectedConditions.elementToBeClickable(save));
        save.click();
        wait.until(ExpectedConditions.visibilityOf(leadAddedPopup));
        leadAddedPopup.click();

    }

    public void addNewLeadSold(String phoneNumber, boolean trial, boolean productInfo, String types, String address, boolean soldPos, boolean sold, String Code, boolean type) {

        wait.until(ExpectedConditions.elementToBeClickable(addConsumer));
        addConsumer.click();
        wait.until(ExpectedConditions.visibilityOf(number));

        Actions actions = new Actions(driver);

        actions.moveToElement(isLead);
        actions.perform();
        if (!isLead.isSelected()) {
            isLead.click();
        }

        actions.moveToElement(number);
        actions.perform();
        number.sendKeys("112");
        wait.until(ExpectedConditions.visibilityOf(element));
        element.click();

        if (tial) {
            if (!this.trial.isSelected()) {
                this.trial.click();
            }
        }
        else {
            if (this.trial.isSelected()) {
                this.trial.click();
            }
        }
        if (productInfo) {
            if (!this.productInfo.isSelected()) {
                this.productInfo.click();
            }
        }
        else {
            if (this.productInfo.isSelected()) {
                this.productInfo.click();
            }
        }
        this.types.sendKeys(types);

        actions.moveToElement(name);
        actions.perform();
        name.sendKeys("Testowy lead");

        actions.moveToElement(phone);
        actions.perform();
        phone.sendKeys(phoneNumber);

        actions.moveToElement(sold);
        actions.perform();
        if (!sold.isSelected()) {
            sold.click();
        }
        actions.moveToElement(this.soldPos);
        actions.perform();
        if (soldPos){
            this.soldPos.click();
        }
        actions.moveToElement(this.user);
        actions.perform();
        if (sold){
            this.user.click();
            Code.sendKeys(Code);
        }

        actions.moveToElement(addressInput);
        actions.perform();
        addressInput.sendKeys(address);
        addressButton.click();
        wait.until(ExpectedConditions.visibilityOf(addressButton));

        actions.moveToElement(agreementCheckbox);
        actions.perform();
        if (!this.agreementCheckbox.isSelected()) {
            this.agreementCheckbox.click();
        }

        actions.moveToElement(this.webElement);
        actions.perform();
        if (type) {
            if (!this.webElement.isSelected()) {
                this.webElement.click();
            }
        }
        else {
            if (this.webElement.isSelected()) {
                this.webElement.click();
            }
        }

        actions.moveToElement(send);
        actions.perform();
        wait.until(ExpectedConditions.elementToBeClickable(send));
        send.click();
        wait.until(ExpectedConditions.visibilityOf(cancel));
        wait.until(ExpectedConditions.elementToBeClickable(save));
        save.click();
        wait.until(ExpectedConditions.visibilityOf(leadAddedPopup));
        leadAddedPopup.click();

    }

    public void addNewQuestionnaire(boolean event, String eventName, boolean trial, boolean productInfo, String types) {

        wait.until(ExpectedConditions.elementToBeClickable(addConsumer));
        addConsumer.click();
        wait.until(ExpectedConditions.visibilityOf(number));

        Actions actions = new Actions(driver);

        actions.moveToElement(isLead);
        actions.perform();
        if (isLead.isSelected()) {
            isLead.click();
        }

        actions.moveToElement(number);
        actions.perform();
        wait.until(ExpectedConditions.visibilityOf(eventCheckbox));
        if (event){
            if (!eventCheckbox.isSelected()) {
                eventCheckbox.click();
            }
            this.eventName.sendKeys(eventName);
        }
        else {
            if (eventCheckbox.isSelected()) {
                eventCheckbox.click();
            }
            number.sendKeys("112");
            wait.until(ExpectedConditions.visibilityOf(element));
            element.click();
        }

        actions.moveToElement(this.trial);
        actions.perform();
        if (trial) {
            if (!this.trial.isSelected()) {
                this.trial.click();
            }
        }
        else {
            if (this.trial.isSelected()) {
                this.trial.click();
            }
        }
        if (productInfo) {
            if (!this.productInfo.isSelected()) {
                this.productInfo.click();
            }
        }
        else {
            if (this.productInfo.isSelected()) {
                this.productInfo.click();
            }
        }
        this.types.sendKeys(types);

        actions.moveToElement(send);
        actions.perform();
        wait.until(ExpectedConditions.elementToBeClickable(send));
        send.click();
        wait.until(ExpectedConditions.visibilityOf(cancel));
        wait.until(ExpectedConditions.elementToBeClickable(save));
        save.click();
        wait.until(ExpectedConditions.visibilityOf(leadAddedPopup));
        leadAddedPopup.click();
    }
    public void addNewLeadForNewUser(String phoneNumber, boolean trial, boolean productInfo, String types, String address, boolean type) {

        wait.until(ExpectedConditions.elementToBeClickable(addConsumer));
        addConsumer.click();
        wait.until(ExpectedConditions.visibilityOf(number));

        Actions actions = new Actions(driver);

        actions.moveToElement(isLead);
        actions.perform();
        if (!isLead.isSelected()) {
            isLead.click();
        }

        actions.moveToElement(number);
        actions.perform();
        number.sendKeys("112");
        wait.until(ExpectedConditions.visibilityOf(element));
        element.click();
        actions.moveToElement(sold);
        actions.perform();
        if (sold.isSelected()) {
            sold.click();
        }

        actions.moveToElement(this.trial);
        actions.perform();
        if (trial) {
            if (!this.trial.isSelected()) {
                this.trial.click();
            }
        }
        else {
            if (this.trial.isSelected()) {
                this.trial.click();
            }
        }
        if (productInfo) {
            if (!this.productInfo.isSelected()) {
                this.productInfo.click();
            }
        }
        else {
            if (this.productInfo.isSelected()) {
                this.productInfo.click();
            }
        }
        this.types.sendKeys(types);

        actions.moveToElement(name);
        actions.perform();
        name.sendKeys("Testowy lead");

        actions.moveToElement(phone);
        actions.perform();
        phone.sendKeys(phoneNumber);

        actions.moveToElement(addressInput);
        actions.perform();
        addressInput.sendKeys(address);
        addressButton.click();
        wait.until(ExpectedConditions.visibilityOf(addressButton));

        actions.moveToElement(agreementCheckbox);
        actions.perform();
        if (!this.agreementCheckbox.isSelected()) {
            this.agreementCheckbox.click();
        }

        actions.moveToElement(this.webElement);
        actions.perform();
        if (type) {
            if (!this.webElement.isSelected()) {
                this.webElement.click();
            }
        }
        else {
            if (this.webElement.isSelected()) {
                this.webElement.click();
            }
        }

        actions.moveToElement(send);
        actions.perform();
        wait.until(ExpectedConditions.elementToBeClickable(send));
        send.click();
        wait.until(ExpectedConditions.visibilityOf(cancel));
        wait.until(ExpectedConditions.elementToBeClickable(save));
        save.click();
        wait.until(ExpectedConditions.visibilityOf(leadAddedPopup));
        leadAddedPopup.click();

    }

}
