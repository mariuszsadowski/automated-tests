

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class UserAPage {

    WebDriver driver;
    WebDriverWait wait;

    public UserAPage(WebDriver driver, WebDriverWait wait) {
        this.driver = driver;
        this.wait = wait;
        PageFactory.initElements(driver, this);
    }

    //Dodaj konsumenta
    @FindBy(xpath = "//span[contains(text(),'Dodaj Konsumenta')]")
    public WebElement addConsumer;

    @FindBy(xpath = "//span[contains(text(),'Imię')]//following-sibling::input")
    public WebElement name;

    @FindBy(xpath = "//span[contains(text(),'Nr telefonu komórkowego')]//following-sibling::input")
    public WebElement phone;

    @FindBy(xpath = "//input[@id='address-input']")
    public WebElement addressInput;

    @FindBy(xpath = "//span[contains(text(),'Sprawdź')]")
    public WebElement addressButton;

    @FindBy(xpath = "//label[contains(text(),'Oświadczam, że znajomy, któremu')]")
    public WebElement agreementCheckbox;

    @FindBy(xpath = "//span[contains(text(),'Wyślij')]")
    public WebElement send;

    @FindBy(xpath = "//button[contains(text(),'Zapisz')]")
    public WebElement save;

    @FindBy(xpath = "//button[contains(text(),'Anuluj')]")
    public WebElement cancel;

    @FindBy(xpath = "//button[contains(text(), 'Ok')]")
    public WebElement leadAddedPopup;

    public void addNewLead(String phoneNumber, String address) {

        Actions actions = new Actions(driver);

        wait.until(ExpectedConditions.elementToBeClickable(addConsumer));
        addConsumer.click();
        wait.until(ExpectedConditions.visibilityOf(name));
        name.sendKeys("Testowy lead");
        phone.sendKeys(phoneNumber);

        actions.moveToElement(addressInput);
        actions.perform();
        addressInput.sendKeys(address);
        addressButton.click();
        wait.until(ExpectedConditions.visibilityOf(addressButton));

        actions.moveToElement(agreementCheckbox);
        actions.perform();
        if (!agreementCheckbox.isSelected()) {
            agreementCheckbox.click();
        }

        actions.moveToElement(send);
        actions.perform();
        wait.until(ExpectedConditions.elementToBeClickable(send));
        send.click();
        wait.until(ExpectedConditions.visibilityOf(cancel));
        wait.until(ExpectedConditions.elementToBeClickable(save));
        save.click();
        wait.until(ExpectedConditions.visibilityOf(leadAddedPopup));
        leadAddedPopup.click();
    }
}