package com.test.iqfe2e.pages.adminpanel;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AdminPanelLoginPage {

    WebDriver driver;
    WebDriverWait wait;

    public AdminPanelLoginPage(WebDriver driver, WebDriverWait wait) {
        this.driver = driver;
        this.wait = wait;
        PageFactory.initElements(driver, this);
    }

    //Login
    @FindBy(xpath = "//input[@id='username']")
    WebElement username;

    @FindBy(xpath = "//input[@id='password']")
    WebElement password;

    @FindBy(xpath = "//button[@type='submit']")
    WebElement submit;

    //Konto
    @FindBy(xpath = "//span[contains(text(),'Konto')]")
    WebElement account;

    //Ustawienia
    @FindBy(xpath = "//span[contains(text(),'Ustawienia')]")
    WebElement settings;

    //Hasło
    @FindBy(xpath = "//span[contains(text(),'Hasło')]")
    WebElement pass;

    //Wyloguj
    @FindBy(xpath = "//span[contains(text(),'Wyloguj')]")
    WebElement logout;

    //Password reset
    @FindBy(xpath = "//span[@class='ng-binding']")
    WebElement dontRememberPassword;

    @FindBy(xpath = "//input[@name='phoneNumber']")
    WebElement emailResetPassword;

    @FindBy(xpath = "//button[@class='button ng-binding button-default']")
    WebElement send;

    public void login(String username, String password) {
        wait.until(ExpectedConditions.elementToBeClickable(submit));
        this.username.sendKeys(username);
        this.password.sendKeys(password);
        submit.click();
    }

    public void logout() throws InterruptedException {
        Thread.sleep(5000);
        wait.until(ExpectedConditions.elementToBeClickable(account));
        account.click();
        wait.until(ExpectedConditions.elementToBeClickable(settings));
        settings.click();
        wait.until(ExpectedConditions.elementToBeClickable(logout));
        logout.click();
        wait.until(ExpectedConditions.elementToBeClickable(submit));
    }

    public void passwordReset(String emailResetPassword) {
        dontRememberPassword.click();
        emailResetPassword.sendKeys(emailResetPassword);
        send.click();
    }
}
