package com.test.iqfe2e.pages.adminpanel;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AdminPanelMainPage {
    WebDriver driver;
    WebDriverWait wait;

    public AdminPanelMainPage(WebDriver driver, WebDriverWait wait) {
        this.driver = driver;
        this.wait = wait;
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//span[contains(text(),'Definicje KPI')]")
    WebElement kpi;

    @FindBy(xpath = "//span[contains(text(),'Administracja')]")
    WebElement administration;

    @FindBy(xpath = "//body/div[1]/div[1]/aside[1]/section[1]/ul[1]/li[19]/ul[1]/li[3]/a[1]")
    WebElement leads;

    @FindBy(xpath = "//a[@ui-sref='user-management']")
    WebElement users;

    @FindBy(xpath = "//span[contains(text(),'Filtry')]")
    WebElement filters;

    @FindBy(xpath = "//div[@id='field_phoneNumber']//input")
    WebElement inputPhoneNumberFilters;

    @FindBy(xpath = "//body/div[1]/div[1]/div[1]/form[1]/div[2]/div[1]/div[1]/div[1]/input[1]")
    WebElement inputIdFilters;

    @FindBy(xpath = "//body/div[1]/div[1]/div[1]/form[1]/div[3]/button[2]/span[1]")
    WebElement confirmationSettingFilters;

    @FindBy(xpath = "//span[contains(text(),'Edytuj')]")
    WebElement editButton;

    @FindBy(xpath = "//body[1]/div[1]/div[1]/div[1]/form[1]/div[3]/button[1]")
    WebElement resignButton;

    @FindBy(xpath = "//select[@id='field_status']")
    WebElement statusLeadEdit;

    @FindBy(xpath = "//body/div[1]/div[1]/div[1]/form[1]/div[2]/div[1]/div[1]/div[1]/select[1]")
    WebElement resignationReasonLeadEdit;

    @FindBy(xpath = "//body/div[1]/div[1]/div[1]/form[1]/div[2]/div[1]/div[1]/sellions-map[1]/div[1]/div[2]/div[1]/input[1]")
    WebElement citySpotLeadEdit;

    @FindBy(xpath = "//span[contains(text(),'Szukaj')]")
    WebElement citySpotConfirmationLeadEdit;

    @FindBy(xpath = "//span[contains(text(),'Zapisz')]")
    WebElement saveLeadEditButton;

    public void resignBuddy (Integer kingId) throws InterruptedException {

        wait.until(ExpectedConditions.elementToBeClickable(administration));
        administration.click();
        wait.until(ExpectedConditions.elementToBeClickable(users));
        users.click();
        wait.until(ExpectedConditions.elementToBeClickable(filters));
        filters.click();
        wait.until(ExpectedConditions.elementToBeClickable(inputIdFilters));
        inputIdFilters.click();
        inputIdFilters.sendKeys(kingId.toString());
        inputIdFilters.sendKeys(Keys.ENTER);
        wait.until(ExpectedConditions.elementToBeClickable(confirmationSettingFilters));
        confirmationSettingFilters.click();
        Thread.sleep(2000);
        wait.until(ExpectedConditions.elementToBeClickable(editButton));
        editButton.click();
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView({block: 'center'});", resignBuddyButton);
        wait.until(ExpectedConditions.elementToBeClickable(resignBuddyButton));
        resignBuddyButton.click();
        Thread.sleep(2000);
    }

    public void leadActivation (String phoneNumber) throws InterruptedException {
        wait.until(ExpectedConditions.elementToBeClickable(administration));
        administration.click();
        wait.until(ExpectedConditions.elementToBeClickable(leads));
        leads.click();
        wait.until(ExpectedConditions.elementToBeClickable(filters));
        filters.click();
        wait.until(ExpectedConditions.elementToBeClickable(inputPhoneNumberFilters));
        inputPhoneNumberFilters.click();
        inputPhoneNumberFilters.sendKeys(phoneNumber);
        inputPhoneNumberFilters.sendKeys(Keys.ENTER);
        wait.until(ExpectedConditions.elementToBeClickable(confirmationSettingFilters));
        confirmationSettingFilters.click();
        wait.until(ExpectedConditions.elementToBeClickable(editButton));
        editButton.click();
        wait.until(ExpectedConditions.elementToBeClickable(statusLeadEdit));
        statusLeadEdit.click();
        statusLeadEdit.sendKeys("Niezarezerwowany");
        statusLeadEdit.sendKeys(Keys.ENTER);
        wait.until(ExpectedConditions.elementToBeClickable(citySpotLeadEdit));
        citySpotLeadEdit.click();
        citySpotLeadEdit.sendKeys("97-300");
        wait.until(ExpectedConditions.elementToBeClickable(citySpotConfirmationLeadEdit));
        citySpotConfirmationLeadEdit.click();
        Thread.sleep(1500);
        wait.until(ExpectedConditions.elementToBeClickable(saveLeadEditButton));
        saveLeadEditButton.click();

    }
}
