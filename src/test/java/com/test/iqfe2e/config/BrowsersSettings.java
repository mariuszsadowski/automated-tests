package com.test.iqfe2e.config;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.util.Map;

public class BrowsersSettings {

    private static final String SELENIUM_GRID_URL = "SELENIUM_GRID_URL";

    public static WebDriver driver;

    public static WebDriver startBrowser(String browserName) {
        if (System.getenv(SELENIUM_GRID_URL) != null) {
            connectToGrid(browserName);
        } else {
            startLocalBrowser(browserName);
        }

        driver.manage().window().maximize();
        return driver;
    }

    private static void connectToGrid(String browserName) {
        driver = RemoteWebDriver.builder()
                .url(System.getenv(SELENIUM_GRID_URL))
                .setCapability(CapabilityType.BROWSER_NAME, browserName)
                .build();
    }

    private static void startLocalBrowser(String browserName) {
        Map<String, String> env = System.getenv();
        if (browserName.equals("firefox")) {
            System.setProperty("webdriver.gecko.driver", env.getOrDefault("WEBDRIVER_GECKO_DRIVER", "geckodriver.exe"));
            driver = new FirefoxDriver();
        } else if(browserName.equals("chrome")) {
            System.setProperty("webdriver.chrome.driver", env.getOrDefault("WEBDRIVER_CHROME_DRIVER","chromedriver.exe"));
            driver = new ChromeDriver();
        }
    }
}
