package com.test.iqfe2e.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Service
public class VariablesService {
    @Autowired
    private Environment env;

    public String getAdminPanelUrl() {
        return env.getProperty("app.admin_panel_url");
    }

    public String getUrl1() {
        return env.getProperty("app.url1");
    }

    public String getUrl2() { return env.getProperty("app.url2"); }

    public String getUrl3() { return env.getProperty("app.url3"); }
}
