package com.test.iqfe2e.regressiontests;

import com.test.iqfe2e.config.RetryAnalyzer;
import com.test.iqfe2e.config.RetryTestListener;
import com.test.iqfe2e.config.VariablesService;
import com.test.iqfe2e.sellions.backend.client.BackendClient;
import com.test.iqfe2e.sellions.backend.dto.NewLeadDTO;
import com.test.iqfe2e.sellions.backend.dto.NewMGMUserDTO;
import com.test.iqfe2e.sellions.engine.client.EngineClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.Map;
import java.util.UUID;
//import com.test.iqfe2e.sellions.backend.dto.RunLastTimerEngine;

@SpringBootTest
@Listeners(RetryTestListener.class)
public class EndpointTests extends AbstractTestNGSpringContextTests {

    @Autowired
    BackendClient backendClient;
    @Autowired
    EngineClient engineClient;

    @Autowired
    VariablesService variablesService;

    @Test(retryAnalyzer= RetryAnalyzer.class)
    public void mirrorTest() {

        System.out.println(variablesService.getAdminLogin());
        System.out.println(variablesService.getAdminPaswword());
        System.out.println(variablesService.getUserPassword());

        NewMGMUserDTO dto = new NewMGMUserDTO(
                String.format("test-%s@test", UUID.randomUUID()),
                "FIRST_NAME",
                "LAST_NAME",
                "999999999"
        );
        Map<String, Object> responseMap = backendClient.createMGMUserByMirror(dto);
        System.out.println(responseMap);
        String userId = responseMap.get("userId").toString();
    }

    @Test(retryAnalyzer = RetryAnalyzer.class)
    public void leadsReferredTest() {
        NewLeadDTO dto = new NewLeadDTO();
        dto.setName("Testowy lead");
        dto.setPhoneNumber("111745887");
        backendClient.createLeadByReferralCode(dto, "TYP7434");
    }
}