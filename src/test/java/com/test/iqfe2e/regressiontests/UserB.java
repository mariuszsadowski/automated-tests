package com.test.iqfe2e.regressiontests;

import com.test.iqfe2e.config.BrowsersSettings;
import com.test.iqfe2e.config.RetryAnalyzer;
import com.test.iqfe2e.config.RetryTestListener;
import com.test.iqfe2e.config.VariablesService;
import com.test.iqfe2e.dbgetter.DataClear;
import com.test.iqfe2e.pages.activation.LeadActivationPage;
import com.test.iqfe2e.pages.activation.UserActivationPage;
import com.test.iqfe2e.pages.adminpanel.AdminPanelLoginPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@SpringBootTest
@Listeners(RetryTestListener.class)
public class UserB extends AbstractTestNGSpringContextTests {


    private static final int TIMEOUT = 10000;

    public WebDriver driver;
    public WebDriverWait wait;
    public DataClear dataClear;

    @Autowired
    VariablesService variablesService;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @BeforeMethod
    public void beforeTest() {
        dataClear = new DataClear();
        driver = BrowsersSettings.startBrowser("chrome");
        wait = new WebDriverWait(driver, 30);
    }

    @AfterMethod
    public void afterTest() {
        driver.quit();
        dataClear.clearData(jdbcTemplate);
    }

    @Test(retryAnalyzer= RetryAnalyzer.class)
    public void createUserAddLeadReserveBy() throws InterruptedException {
        driver.get(variablesService.getAdminPanelUrl());

        AdminPanelLoginPage adminPanelLoginPage = new AdminPanelLoginPage(driver, wait);
        adminPanelLoginPage.login(variablesService.getAdminLogin(), variablesService.getAdminPaswword());

        AdminPanelUsersPage adminPanelUsersPage = new AdminPanelUsersPage(driver, wait);
        dataClear.addUserToCleanUp(jdbcTemplate, variablesService.getEmailDeletable());
        adminPanelUsersPage.addNewUser(jdbcTemplate, variablesService.getEmailDeletable(), "User");

        Thread.sleep(TIMEOUT);

        UserActivationPage userActivationPage = new UserActivationPage(driver, wait);
        userActivationPage.activateUser(jdbcTemplate, variablesService.getEmailDeletable(),"Mail", variablesService.getUserPassword(), variablesService.getAdminPanelUrl());

        driver.get(variablesService.getUrl());

        LoginPage loginPage = new LoginPage(driver, wait);
        loginPage.login(variablesService.getEmailDeletable(), variablesService.getUserPassword());

        UserBPage userBPage = new UserBPage(driver, wait);
        dataClear.addLeadToCleanUp(jdbcTemplate, variablesService.getLeadPhoneNumberDeletable());
        userBPage.addNewLeadForNewUser(variablesService.getLeadPhoneNumberDeletable(), false, false, "A", "Puławy",false);
        loginPage.logout();

        Thread.sleep(TIMEOUT);

        LeadActivationPage leadActivationPage = new LeadActivationPage(driver, wait);
        leadActivationPage.activateLead(jdbcTemplate,variablesService.getLeadPhoneNumberDeletable(),"A");

        driver.get(variablesService.getUrl());

        loginPage.login(variablesService.getEmail(), variablesService.getUserPassword());
        loginPage.logout();
    }

    @Test(retryAnalyzer= RetryAnalyzer.class)
    public void addLeadEvent() {
        driver.get(variablesService.getUrl());

        LoginPage loginPage = new LoginPage(driver, wait);
        loginPage.login(variablesService.getEmail(), variablesService.getUserPassword());

        UserBPage userBPage = new UserBPage(driver, wait);
        dataClear.addLeadToCleanUp(jdbcTemplate, variablesService.getLeadPhoneNumberDeletable());
        userBPage.addNewLead(variablesService.getLeadPhoneNumberDeletable(), true, "wydarzenie998", true, true, "A", "Puławy",true);
        loginPage.logout();
    }

    @Test(retryAnalyzer= RetryAnalyzer.class)
    public void addLead() {
        driver.get(variablesService.getUrl());

        LoginPage loginPage = new LoginPage(driver, wait);
        loginPage.login(variablesService.getEmail(), variablesService.getUserPassword());

        UserBPage userBPage = new UserBPage(driver, wait);
        dataClear.addLeadToCleanUp(jdbcTemplate, variablesService.getLeadPhoneNumberDeletable());
        userBPage.addNewLead(variablesService.getLeadPhoneNumberDeletable(), false, "wydarzenie998", false, true, "A", "Puławy",false);
        loginPage.logout();
    }

    @Test(retryAnalyzer= RetryAnalyzer.class)
    public void addLeadSoldPos() {
        driver.get(variablesService.getUrl());

        LoginPage loginPage = new LoginPage(driver, wait);
        loginPage.login(variablesService.getEmail(), variablesService.getUserPassword());

        UserBPage userBPage = new UserBPage(driver, wait);
        dataClear.addLeadToCleanUp(jdbcTemplate, variablesService.getLeadPhoneNumberDeletable());
        userBPage.addNewLeadSold(variablesService.getLeadPhoneNumberDeletable(), true, false, "A", "Puławy",true, false,null, true);
        loginPage.logout();
    }

    @Test(retryAnalyzer= RetryAnalyzer.class)
    public void addLead() {
        driver.get(variablesService.getUrl());

        loginPage loginPage = new loginPage(driver, wait);
        loginPage.login(variablesService.getEmail(), variablesService.getUserPassword());

        UserBPage userBPage = new UserBPage(driver, wait);
        dataClear.addLeadToCleanUp(jdbcTemplate, variablesService.getLeadPhoneNumberDeletable());
        userBPage.addNewLeadSold(variablesService.getLeadPhoneNumberDeletable(), true, true, "A", "Puławy",false, true,"id:48932442", false);
        loginPage.logout();
    }

    @Test(retryAnalyzer= RetryAnalyzer.class)
    public void addQuestionnaireEvent() {
        driver.get(variablesService.getUrl());

        LoginPage lLoginPage = new LoginPage(driver, wait);
        loginPage.login(variablesService.getEmail(), variablesService.getUserPassword());

        UserBPage userBPage = new UserBPage(driver, wait);
        userBPage.addNewQuestionnaire(true, "wydarzenie998", false, true, "A");
        loginPage.logout();
    }
    @Test(retryAnalyzer= RetryAnalyzer.class)
    public void addNewQuestionnaire() {
        driver.get(variablesService.getUrl());

        loginPage loginPage = new LoginPage(driver, wait);
        loginPage.login(variablesService.getEmail(), variablesService.getUserPassword());

        UserBPage userBPage = new UserBPage(driver, wait);
        userBPage.addNewQuestionnaire(false, "wydarzenie998", true, false, "A");
        loginPage.logout();
    }
}
