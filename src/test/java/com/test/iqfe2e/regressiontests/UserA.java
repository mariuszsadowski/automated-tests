package com.test.iqfe2e.regressiontests;

import com.test.iqfe2e.config.BrowsersSettings;
import com.test.iqfe2e.config.RetryAnalyzer;
import com.test.iqfe2e.config.RetryTestListener;
import com.test.iqfe2e.config.VariablesService;
import com.test.iqfe2e.dbgetter.DataClear;
import com.test.iqfe2e.pages.activation.LeadActivationPage;
import com.test.iqfe2e.pages.activation.UserActivationAgrPage;
import com.test.iqfe2e.pages.adminpanel.AdminPanelLoginPage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.*;

@SpringBootTest
@Listeners(RetryTestListener.class)
public class UserA extends AbstractTestNGSpringContextTests {
    private static final int TIMEOUT = 10000;

    public WebDriver driver;
    public WebDriverWait wait;
    public DataClear dataClear;

    @Autowired
    VariablesService variablesService;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @BeforeMethod
    public void beforeTest() {
        dataClear = new DataClear();
        driver = BrowsersSettings.startBrowser("chrome");
        wait = new WebDriverWait(driver, 30);
    }

    @AfterMethod
    public void afterTest() {
        driver.quit();
        dataClear.clearData(jdbcTemplate);
    }

    @Test(retryAnalyzer= RetryAnalyzer.class)
    public void createUserAddLeadReserveBy() throws InterruptedException {
        driver.get(variablesService.getAdminPanelUrl());

        AdminPanelLoginPage adminPanelLoginPage = new AdminPanelLoginPage(driver, wait);
        adminPanelLoginPage.login(variablesService.getAdminLogin(), variablesService.getAdminPaswword());

        AdminPanelUsersPage adminPanelUsersPage = new AdminPanelUsersPage(driver, wait);
        dataClear.addUserToCleanUp(jdbcTemplate, variablesService.getEmailDeletable());
        adminPanelUsersPage.addNewUser(jdbcTemplate ,variablesService.getEmailDeletable(), "B");
        //adminPanelLoginPage.logout();

        Thread.sleep(TIMEOUT);

        UserActivationAgrPage userActivationPage = new UserActivationAgrPage(driver, wait);
        userActivationPage.activateUserAgr(jdbcTemplate, variablesService.getEmailDeletable(), "Activation",variablesService.getUserPassword(), variablesService.getAdminPanelUrl());

        driver.get(variablesService.getUrl());

        LoginPage loginPage = new LoginPage(driver, wait);
        LoginPage.login(variablesService.getEmailDeletable(), variablesService.getUserPassword());

        UserAPage userAPage = new UserAPage(driver, wait);
        dataClear.addLeadToCleanUp(jdbcTemplate, variablesService.getLeadPhoneNumberDeletable());
        userAPage.addNewLead(variablesService.getLeadPhoneNumberDeletable(),"Siedlce");
        loginPage.logout();

        Thread.sleep(TIMEOUT);

        LeadActivationPage leadActivationPage = new LeadActivationPage(driver, wait);
        leadActivationPage.activateLead(jdbcTemplate,variablesService.getLeadPhoneNumberDeletable(),"A");

        driver.get(variablesService.getUrl());
        loginPage.login(variablesService.getEmail(), variablesService.getUserPassword());
        loginPage.logout();
    }

    @Test(retryAnalyzer= RetryAnalyzer.class)
    public void addLead()  {
        driver.get(variablesService.getUrl());

        LoginPage loginPage = new LoginPage(driver, wait);
        loginPage.login(variablesService.getEmail(), variablesService.getUserPassword());

        UserAPage userAPage = new UserAPage(driver, wait);
        dataClear.addLeadToCleanUp(jdbcTemplate, variablesService.getLeadPhoneNumberDeletable());
        userAPage.addNewLead(variablesService.getLeadPhoneNumberDeletable(),"Siedlce");
        loginPage.logout();
    }
}