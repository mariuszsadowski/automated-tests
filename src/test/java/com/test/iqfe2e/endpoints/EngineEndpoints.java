package com.test.iqfe2e.endpoints;

import com.test.iqfe2e.sellions.engine.client.EngineClient;
import org.springframework.beans.factory.annotation.Autowired;

public class EngineEndpoints {

    @Autowired
    EngineClient engineClient;

    public void runSoldShortNps(EngineClient engineClient) {
        engineClient.runTimerAfterLeadSoldShort();
    }

    public void runSoldLongNps(EngineClient engineClient) {
        engineClient.runTimerAfterLeadSoldLong();
    }

}
