package com.test.iqfe2e.endpoints;

import com.test.iqfe2e.sellions.backend.client.BackendClient;
import com.test.iqfe2e.sellions.backend.dto.NewLeadDTO;
import org.springframework.beans.factory.annotation.Autowired;

public class External {

    @Autowired
    BackendClient backendClient;

    public void newLeadByRefferralCode(String phoneNumber, String Code, BackendClient backendClient) {
        NewLeadDTO dto = new NewLeadDTO();

        dto.setName("Test Automatyczny");
        dto.setPhoneNumber(phoneNumber);


        backendClient.createLead(dto, referralCode);
    }
}
